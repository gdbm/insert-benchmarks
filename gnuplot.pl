# This file is part of GDBM insert-benchmarks
# Copyright (C) 2022 Sergey Poznyakoff
#
# This file is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This file is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this file.  If not, see <http://www.gnu.org/licenses/>.

use strict;
use warnings;
use File::Spec;

my $build_dir = 'build';
my $benchmark_topdir = 'benchmarks';
my $datafile = 'inskeys.log';

sub describe {
    my $name = shift;
    my $dir = File::Spec->catfile($build_dir, $name, '.git');
    my @cmd = ('git', '-C', $dir, qw(log -n 1 --format=%h));
    open(my $fh, '-|', @cmd) or die "can't run git in $dir";
    chomp(my $res = <$fh>);
    close $fh;
    $res;
}
    
sub print_plot {
    my $n = 0;
    my @a = map {
	++$n;
	my $name = describe($_);
	if ($name) {
	    $name = "$_ ($name)";
	} else {
	    $name = $_;
	}
	sprintf("'%s' using 2:1 title '%s' smooth bezier ls %d",
		File::Spec->catfile($benchmark_topdir, $_, $datafile),
	        $name,
	        $n);
    } @_;
    print "plot " . join(",\\\n\t", @a) . "\n";
}

push @ARGV, split(/\s+/, $ENV{TARGETS}) unless @ARGV;

print <<EOF;
set title 'Number of inserted keys as function of time'
set xlabel "Seconds"
set ylabel "Keys"
set key below left vertical
EOF
;

print_plot @ARGV;
	    
