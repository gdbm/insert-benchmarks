/*
  NAME
    inskeys - measure GDBM insertion times

  SYNOPSIS
    inskeys [-nqV] [-f INFILE] [-l LOGFILE] [-o DBNAME] [-r N]

  DESCRIPTION
    Reads key/value pairs from INFILE (default: standard input) and
    stores them in the GDBM database DBNAME (default: a.db).  After
    storing each N new records, prints to the LOGFILE (default: standard
    error) the number of records stored so far and the time elapsed since
    program startup (with nanosecond precision).

  OPTIONS
    -n   Disable mmap.

    -f INFILE
         Read input from INFILE instead of from the standard input.

    -l LOGFILE
         Write log to LOGFILE instead of to the standard error.

    -o DBNAME
         Name of the output database (default: a.db).

    -q   Query each key before attempting to store it.

    -r N
         Print log line each N records stored (default: 1000).  N can be
	 suffixed with the following letters (case-insensitive): K (kilobytes),
	 M (megabytes), and G (gigabytes).

    -V
         Print GDBM version with which the program is compiled and linked.

  LICENSE
    This file is part of GDBM insert-benchmarks
    Copyright (C) 2022 Sergey Poznyakoff

    This file is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3, or (at your option)
    any later version.

    This file is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this file.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <unistd.h>
#include <time.h>
#include <string.h>
#include <errno.h>
#include <assert.h>
#include <arpa/inet.h>
#include <gdbm.h>

char *dbname = "a.db";

void
dberror (char const *fmt, ...)
{
  int ec = errno;

  fprintf (stderr, "%s", dbname);
  if (fmt)
    {
      va_list ap;

      fprintf (stderr, ": ");
      va_start (ap, fmt);
      vfprintf (stderr, fmt, ap);
      va_end (ap);
    }
  fprintf (stderr, ": %s", gdbm_strerror (gdbm_errno));
  if (gdbm_check_syserr (gdbm_errno))
    fprintf (stderr, ": %s", strerror (ec));
  fputc ('\n', stderr);
}

#define SIZE_T_MAX ((size_t)-1)

static size_t
to_size (char const *arg)
{
  char *p;
  unsigned long n;
  size_t size;
  
  errno = 0;
  n = strtoul (arg, &p, 10);
  if (errno)
    {
      fprintf (stderr, "invalid size: %s\n", optarg);
      exit(2);
    }
  size = n;
  if (*p)
    {
      if (p[1])
	{
	  fprintf (stderr, "invalid size suffix: %s\n", arg);
	  exit (2);
	}

#     define XB()							\
      if (SIZE_T_MAX / size < 1024) 					\
	{								\
	  fprintf (stderr,						\
		   "size out of range: %s\n",				\
		   optarg);						\
	  exit (2);							\
	}								\
      size <<= 10;

      switch (*p)
	{
	case 'g':
	case 'G':
	  XB();
	  /* fall through */
	case 'm':
	case 'M':
	  XB();
	  /* fall through */
	case 'k':
	case 'K':
	  XB();
	  break;
	  
	default:
	  fprintf (stderr, "invalid size suffix: %s\n", arg);
	  exit (2);
	}
    }
  return size;
}

static struct timespec
ts_sub (struct timespec a, struct timespec b)
{
  struct timespec diff;

  diff.tv_sec = a.tv_sec - b.tv_sec;
  diff.tv_nsec = a.tv_nsec - b.tv_nsec;
  if (diff.tv_nsec < 0)
    {
      --diff.tv_sec;
      diff.tv_nsec += 1e9;
    }
  
  return diff;
}

uint32_t
read_size (FILE *fp)
{
  uint32_t size;
  if (fread (&size, sizeof (size), 1, fp) != 1)
    {
      if (ferror (fp))
	{
	  perror ("read_size");
	  abort ();
	}
      return 0;
    }
  return ntohl (size);
}

int
read_record (FILE *fp, datum *key, datum *content)
{
  ssize_t len;
  size_t n;
  char *ptr;

  n = 0;
  ptr = NULL;
  len = getdelim (&ptr, &n, '\t', fp);
  if (len <= 0)
    return 0;

  key->dptr = ptr;
  key->dsize = len-1;

  n = 0;
  ptr = NULL;
  len = getdelim (&ptr, &n, '\n', fp);
  if (len <= 0)
    {
      free (key->dptr);
      return 0;
    }

  content->dptr = ptr;
  content->dsize = len-1;
  return 1;
}

int
main (int argc, char **argv)
{
  GDBM_FILE dbf;
  int flags = GDBM_NEWDB;
  struct timespec start;
  char *infilename = NULL;
  FILE *fp;
  size_t n;
  size_t report_records = 1000;
  int i;
  datum key, content;
  int query_key = 0;
  FILE *log = stdout;
  size_t cache_size = 0;
  size_t start_keys = 0;
  
  while ((i = getopt (argc, argv, "c:nf:kl:o:qr:V")) != EOF)
    {
      switch (i)
	{
	case 'c':
	  cache_size = to_size (optarg);
	  break;

	case 'k':
	  flags = GDBM_WRCREAT;
	  break;
	  
	case 'n':
	  flags |= GDBM_NOMMAP;
	  break;

	case 'f':
	  infilename = optarg;
	  break;

	case 'l':
	  log = fopen(optarg, "w");
	  if (!log)
	    {
	      perror(optarg);
	      exit (2);
	    }
	  break;
	  
	case 'o':
	  dbname = optarg;
	  break;

	case 'q':
	  query_key = 1;
	  break;
	  
	case 'r':
	  if ((report_records = to_size(optarg)) == 0)
	    {
	      fprintf (stderr, "argument to -r must be positive\n");
	      exit (2);
	    }
	  break;

	case 'V':
	  printf ("genkeys built for:\n");
	  printf ("gdbm version: %d.%d",
		  GDBM_VERSION_MAJOR,
		  GDBM_VERSION_MINOR);
#if defined(GDBM_VERSION_PATCH)
# if GDBM_VERSION_PATCH > 0	  
	  printf (".%d", GDBM_VERSION_PATCH);
# endif	  
#endif
	  putchar ('\n');
	  printf ("gdbm library version: %d.%d",
		  gdbm_version_number[0],
		  gdbm_version_number[1]);
	  if (gdbm_version_number[2])
	    printf(".%d", gdbm_version_number[2]);
	  putchar ('\n');	  
	  exit (0);
	    
	default:
	  exit (2);
	}
    }

  if (argc != optind)
    {
      fprintf (stderr, "too many arguments\n");
      exit (2);
    }

  if (infilename)
    {
      fp = fopen (infilename, "r");
      if (!fp)
	{
	  perror (infilename);
	  exit (1);
	}
    }
  else
    fp = stdin;
      
  dbf = gdbm_open (dbname, 0, flags, 00664, NULL);
  if (!dbf)
    {
      dberror (NULL);
      exit (1);
    }
  if (cache_size)
    {
      if (gdbm_setopt (dbf, GDBM_SETCACHESIZE, &cache_size,
		       sizeof (cache_size)))
	{
	  dberror ("GDBM_SETCACHESIZE");
	  exit (1);
	}
    }
  if ((flags & GDBM_OPENMASK) == GDBM_WRCREAT)
    {
      gdbm_count_t n;
      if (gdbm_count (dbf, &n))
	{
	  dberror ("gdbm_count");
	  exit (1);
	}
      if (n > (size_t)-1)
	{
	  fprintf (stderr, "too many records in the database\n");
	  exit (1);
	}
      start_keys = n;
    }
      
  clock_gettime (CLOCK_MONOTONIC, &start);
  n = 0;

  while (read_record (fp, &key, &content))
    {
      n++;
      
      if ((n % report_records) == 0)
	{
	  struct timespec d, now;
	  clock_gettime (CLOCK_MONOTONIC, &now);
	  d = ts_sub (now, start);
	  fprintf (log, "%zu %ld.%09ld\n", start_keys + n, d.tv_sec, d.tv_nsec);
	}

      if (query_key)
	{
	  datum val = gdbm_fetch (dbf, key);
	  if (val.dptr == NULL)
	    {
	      if (gdbm_errno != GDBM_ITEM_NOT_FOUND)
		{
		  dberror ("failed to fetch key %zu", n);
		  exit (1);
		}
	    }
	  else
	    {
	      if (!(val.dsize == content.dsize &&
		    memcmp (val.dptr, content.dptr, val.dsize) == 0))
		{
		  fprintf (stderr, "key %zu value mismatch\n", n);
		  abort ();
		}
	      free (val.dptr);
	    }
	}
      
      switch (gdbm_store (dbf, key, content, GDBM_INSERT))
	{
	case 0:
	case 1:
	  break;

	case -1:
	  dberror ("can't insert datum %zu", n);
	  exit (1);
	}
      free (key.dptr);
      free (content.dptr);
    }
  gdbm_close (dbf);
  fclose (log);
  return 0;
}
	  

  
  
