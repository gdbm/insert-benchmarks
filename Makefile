# This file is part of GDBM insert-benchmarks
# Copyright (C) 2022 Sergey Poznyakoff
#
# This file is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This file is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this file.  If not, see <http://www.gnu.org/licenses/>.

TARGETS := v1.18 v1.19 v1.22 master
TOOLS := inskeys
CONFIGURE_OPTION :=

ifeq (,$(wildcard config.mk))
bootstrap: config.mk
	$(MAKE)

config.mk: Makefile
	@echo "Creating file config.mk with default settings"
	@echo "Edit it if needed"
	@{ echo "## Number of keys to insert"; \
           echo "NKEYS := 100000000"; \
           echo "## Additional options for benchmarking.  See inskeys.c for a discussion of these"; \
           echo "INSKEYS_OPTIONS := $(INSKEYS_OPTIONS)"; \
           echo "## List of GDBM versions (git treeish) to benchmark."; \
           echo "## NOTE: 'master' is added at the beginning unconditionally"; \
	   echo "TARGETS := $(TARGETS)"; \
           echo "## Options to ./configure for all gdbm versions"; \
           echo "CONFIGURE_OPTIONS := $(CONFIGURE_OPTIONS)"; \
           echo "## C flags (will be appended to CONFIGURE_OPTIONS too)"; \
	   echo "CFLAGS = $(CFLAGS)"; \
	} > config.mk
else
  include config.mk
endif

TARGETS := master $(filter-out master,$(TARGETS))

CONFIGURE_OPTIONS += $(CFLAGS)

LOGFILES := $(foreach tgt,$(TARGETS),benchmarks/$(tgt)/inskeys.log)

all: logs benchmark.gnuplot
	gnuplot -p benchmark.gnuplot

benchmark.gnuplot: gnuplot.pl Makefile config.mk
	@perl gnuplot.pl $(TARGETS) > benchmark.gnuplot

logs: $(LOGFILES)

libgdbm = build/$(1)/src/.libs/libgdbm.a
GENKEYS := build/master/tests/num2word

genkeys = $(GENKEYS) -revert 1:$(1)

build: $(GENKEYS) \
     $(foreach tgt,$(TARGETS),$(call libgdbm,$(tgt))) \
     $(foreach tgt,$(TARGETS),\
        $(foreach tool,$(TOOLS),benchmarks/$(tgt)/$(tool))) \

# ====================
# Generate clean goals
# ====================

# Clean logs
define clean_tgt =
clean_$(1):
	rm -f benchmarks/$(1)/inskeys.log
endef

$(foreach tgt,$(TARGETS),$(eval $(call clean_tgt,$(tgt))))

clean: $(foreach tgt,$(TARGETS),clean_$(tgt))
	rm -f benchmark.gnuplot

# Clean tools
define toolclean_tgt = 
toolclean_$(1):
	rm -f $(foreach tool,$(TOOLS),benchmarks/$(1)/$(tool))
endef

$(foreach tgt,$(TARGETS),$(eval $(call toolclean_tgt,$(tgt))))

toolclean: $(foreach tgt,$(TARGETS),clean_$(tgt))

# Clean all built files
define buildclean_tgt =
buildclean_$(1): toolclean_$(1)
	$(MAKE) -C build/$(1) clean
endef

$(foreach tgt,$(TARGETS),$(eval $(call buildclean_tgt,$(tgt))))

buildclean: $(foreach tgt,$(TARGETS),buildclean_$(tgt))

# Clean up everything
allclean:
	rm -rf $(foreach tgt,$(TARGETS),build/$(tgt) benchmarks/$(tgt))

all-clean: allclean

# ====================
# Generate clone goals
# ====================
gdbm/configure.ac:
	git submodule init
	git submodule update

define clone_tgt =
build/$(1)/configure.ac: gdbm/configure.ac
	@echo Cloning $(1)...
	@mkdir -p build/$(1)
	git clone -b $(1) gdbm build/$(1)
endef

$(foreach tgt,$(TARGETS),\
$(eval $(call clone_tgt,$(tgt))))

# ============================
# Generate bootstrapping goals
# ============================
define bootstrap_tgt =
build/$(1)/configure: build/$(1)/configure.ac
	@echo Bootstrapping $(1)...
	@(cd build/$(1) && ./bootstrap)
endef

$(foreach tgt,$(TARGETS),\
$(eval $(call bootstrap_tgt,$(tgt))))

# ========================
# Generate configure goals
# ========================
define configure_tgt =
build/$(1)/Makefile: build/$(1)/configure build/$(1)/Makefile.am
	@echo Configuring $(1)...
	@(cd build/$(1) && ./configure $(CONFIGURE_OPTIONS))
endef

$(foreach tgt,$(TARGETS),\
$(eval $(call configure_tgt,$(tgt))))

# ====================
# Generate build goals
# ====================
define build_tgt =
$(call libgdbm,$(1)): build/$(1)/Makefile
	@echo Building $(1)...
	@make -C build/$(1)
endef

$(foreach tgt,$(TARGETS),\
$(eval $(call build_tgt,$(tgt))))

$(GENKEYS): $(call libgdbm,master)
	@echo Building $(GENKEYS)
	@make -C $(dir $(GENKEYS)) $(notdir $(GENKEYS))

# ==================================
# Generate build goals for the tools
# ==================================
define build_tool =
benchmarks/$(1)/$(2): $(2).c $(call libgdbm,$(1))
	mkdir -p benchmarks/$(1)
	$(CC) $(CPPFLAGS) $(CFLAGS) -Ibuild/$(1)/src -obenchmarks/$(1)/$(2) $(2).c $(call libgdbm,$(1))
endef

$(foreach tgt,$(TARGETS),\
  $(foreach tool,$(TOOLS),$(eval $(call build_tool,$(tgt),$(tool)))))

# =================================
# Generate build goals for the logs
# =================================
define generate_log =
benchmarks/$(1)/$(2): config.mk $(GENKEYS) benchmarks/$(1)/inskeys
	@echo Generating $(2) for $(1)...
	@$(call genkeys,$(NKEYS)) | benchmarks/$(1)/inskeys -obenchmarks/$(1)/a.db -lbenchmarks/$(1)/$(2) $(3)
endef

$(foreach tgt,$(TARGETS),\
  $(eval $(call generate_log,$(tgt),inskeys.log,$(INSKEYS_OPTIONS))))

